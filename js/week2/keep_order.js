const keepOrder = (ary, val) => {
  let index = 0;
  for(let i = 0; i < ary.length; i++) {
    if(val > ary[i]) {
      index = i + 1;
    }
  }
  return index;
}

module.exports = keepOrder
