const balanceStatements = (str) => {
  let sentenceArr = str.split(', ');
  let badArr = [];
  let sumBuy = 0;
  let sumSell = 0;
  
  for(i = 0; i < sentenceArr.length; i++) {
    let wordArr = sentenceArr[i].split(' ');

    if(wordArr[1].match(/^\d+$/)) {
      if(wordArr[3] === 'B') {
        let num1 = wordArr[1];
        let num2 = wordArr[2];
        let result = num1 * num2;
        sumBuy += result;
      } else {
        let num1 = wordArr[1];
        let num2 = wordArr[2];
        let result = num1 * num2;
        sumSell += result;
      }
    } else {
      badArr.push(sentenceArr[i]);
    }
  }

  if(!badArr.length) {
    return `Buy: ${sumBuy} Sell: ${sumSell}`;
  } else {
    return `Buy: ${sumBuy} Sell: ${sumSell}; Badly formed ${badArr.length}: ${badArr} ;`;
  }
}

module.exports = balanceStatements
