const keepOrder = require('../keep_order')

test('Should return 4 when arry is [1, 2, 3, 4, 7] and val is 5', () => {
  expect(keepOrder([1, 2, 3, 4, 7], 5)).toEqual(4)
})

test('Should return 0 when list is [1, 2, 3, 4, 7] and val is 0', () => {
  expect(keepOrder([1, 2, 3, 4, 7], 0)).toEqual(0)
})

test('Should return 2 when list is [1, 1, 2, 2, 2] and val is 2', () => {
  expect(keepOrder([1, 1, 2, 2, 2], 2)).toEqual(2)
})

test('Should return [0, -1] when list is [-1]', () => {
  expect(keepOrder([1, 2, 3, 4], -1)).toEqual(0)
})

test('Should return 1 when list is [1, 2, 3, 4] and val is 2', () => {
  expect(keepOrder([1, 2, 3, 4], 2)).toEqual(1)
})

test('Should return 0 when list is [] and val is 2', () => {
  expect(keepOrder([], 2)).toEqual(0)
})
