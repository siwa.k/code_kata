function reverseOrder(list) {
  let reverseArr = [];
  for(let i = list.length - 1; i >= 0; i--) {
    reverseArr.push(list[i]);
  }
  return reverseArr;
}

module.exports = reverseOrder
