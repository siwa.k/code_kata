require_relative '../max_number'

describe '' do
  it 'Should return 321 when number is 213' do
    assert_equal max_number(213), 321
  end

  it 'Should return 97_632 when number is 63_729' do
    assert_equal max_number(63_729), 97_632
  end

  it 'Should return 977_665 when number is 566_797' do
    assert_equal max_number(566_797), 977_665
  end

  it 'Should return 8_754_321 when number is 2_478_315' do
    assert_equal max_number(2_478_315), 8_754_321
  end

  it 'Should return 76_432 when number is 34_672' do
    assert_equal max_number(34_672), 76_432
  end
end
